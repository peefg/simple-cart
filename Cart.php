<?php

/**
 * Description of Cart
 *
 * @author Peter Seman <seman.peter at gmail.com>
 */
include_once 'Item.php';

class Cart
{
   
    /** @var string our reserved session name */
    const CART_SESSION_CONST = "fds44D__6f4s5fs3";
    
    /** @var array holds cart items */
    private $items = [];
    
    /**
     * Gets "instance" of cart from session or create new one if not exist
     * 
     * @return \Cart        object of cart
     * 
     * @throws Exception    in case of someone has been using our session name...
     */
    public static function getCart() : Cart
    {
        if (!isset($_SESSION[static::CART_SESSION_CONST])) {
            return new Cart();
        }
        $unserialized = unserialize($_SESSION[static::CART_SESSION_CONST]);
        
        if (!$unserialized instanceof Cart) {
            throw new Exception("Not instance of cart");
        }
        
        return $unserialized;
    }
    
    /**
     * 
     * @param Item $item
     */
    public function addItem(Item $item) : void
    {
        $this->items[$item->getSpecialKey()] = $item;
        $this->updateCart();
    }

    /**
     * No idea what is it supposed to do...
     * 
     * @todo NO IDEA
     * 
     * @param Item $item
     */
    public function updateItem(Item $item)
    {
        // srry, let's have it as @todo
    }

    /**
     * Removes item from cart
     * 
     * @param string $itemkey   item key to delete
     * 
     * @return bool             whether has been deleted or not
     */
    public function deleteItem(string $itemkey) : bool
    {

        $deleted = false;
        
        // value not used actualyl
        foreach ($this->items as $key => $_) {
            if ($key == $itemkey) {
                unset($this->items[$itemkey]);
                $this->updateCart();
                $deleted = true;
            }
        }
        
        return $deleted;
        
    }

    /**
     * Purges whole cart
     */
    public function deleteAll() : void
    {
        $this->items = [];
        $this->updateCart();
    }
    
    /**
     * Getter for items
     * 
     * @return array    array of items
     */
    public function getItems() : array
    {
        return $this->items;
    }
    
    /**
     * Calculate sum
     * 
     * @return float total amount of money in cart
     */
    public function getSumOfCart() : float
    {
        $sum = 0;
        
        foreach ($this->items as $item) {
            $sum += $item->getPrice();
        }
        
        return $sum;
    }
    
    /**
     * Just updates the volume of cart after action
     */
    private function updateCart() : void
    {
        $_SESSION[static::CART_SESSION_CONST] = serialize($this);
    }
    
}
