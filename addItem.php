<?php

session_start();
    
include_once 'Cart.php';
include_once 'DBItems.php';

$cart = Cart::getCart();

$dbItems = new DBItems();

$item = $dbItems->getDbItem($_GET['id'] ?? 0);
$cart->addItem($item);

// for case of frontend tests just redirect
header('location: page.php');