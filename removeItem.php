<?php

session_start();
    
include_once 'Cart.php';

$cart = Cart::getCart();

$cart->deleteItem($_GET['id'] ?? 0);

// for case of frontend tests just redirect
header('location: page.php');