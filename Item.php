<?php

/**
 * Description of Item
 *
 * @author Peter Seman <seman.peter at gmail.com>
 */
class Item
{
    /** @var int internal id of item */
    private $id;
    
    /** @var string holds item's name */
    private $name;
    
    /** @var float holds item's price */
    private $price;
    
    /** @var string special key to achieve putting unique items to cart */
    private $specialKey;
    
    /**
     * COnstructor.
     * 
     * @param int $id       item id
     * 
     * @param string $name  itema name
     * 
     * @param float $price  item price
     */
    public function __construct(int $id, string $name, float $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->specialKey = md5(time());
    }

    /**
     * Getter
     * 
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * Getter
     * 
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * Getter
     * 
     * @return float
     */
    public function getPrice() : float
    {
        return $this->price;
    }
    
    /**
     * Getter
     * 
     * @return string
     */
    function getSpecialKey() : string
    {
        return $this->specialKey;
    }

    /**
     * Setter
     * 
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Setter
     * 
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Setter
     * 
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }
    
    /**
     * Setter
     * 
     * @param string $specialKey
     */
    public function setSpecialKey(string $specialKey)
    {
        $this->specialKey = $specialKey;
    }
    
    /**
     * Getter
     * 
     * @return string
     */
    public function getNiceNameWithPrice() : string  
    {
        return $this->name . " | " . $this->price."€";
    }


}
