# Simple Cart

Simple cart is tiny app to show up how to add/remove items from shopping cart. Without frontend and any frameworks.

# Requirements

  - PHP 7.x installed (no special modules are needed)
  - Web server (Apache)

# Settings
 - No settings ara available
  
# Running the app

 - You can start app by calling page.php in an relative call.
```sh
$ http://yourlocalhost/page.php

```