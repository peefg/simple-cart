<?php

session_start();
    
include_once 'Cart.php';

$cart = Cart::getCart();

$cart->deleteAll();

// for case of frontend tests just redirect
header('location: page.php');