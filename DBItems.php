<?php
/**
 * Description of DBItems
 *
 * Mock class... mocking DB
 * 
 * @author Peter Seman <seman.peter at gmail.com>
 */
class DBItems
{

    /** @var array list of items */
    private $items = [];
    
    /**
     * Very simple constructor for mocking available items
     */
    public function __construct()
    {
        $this->items = [
            new Item(1, "Ahoj", 13),
            new Item(2, "Ahoj 2", 15),
            new Item(3, "Ahoj 3", 129),
        ];
    }
    
    /**
     * Gets items from DB
     * 
     * @return array
     */
    public function getDbItems() : array
    {
        return $this->items;
    }
    
    /**
     * Like getter FROM db - just mock
     * 
     * @param int $itemId
     * 
     * @return \Item
     * 
     * @throws Exception    in case of missing such item, probably wrong input
     */
    public function getDbItem(int $itemId) : Item
    {
        foreach ($this->items as $key => $item) {
            if ($item->getId() == $itemId) {
                return $this->items[$key];
            }
        }
        
        throw new Exception("Such item doesn't exist");
    }
}
